import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { AppState } from '../app.module';
import { Store } from '@ngrx/store';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destino-viajes-state';
import { Injectable } from '@angular/core';

@Injectable()
export class DestinoApiClient {

    destinos: DestinoViaje[];
    current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

    constructor(private store: Store<AppState>){
        this.destinos = [];
    }

    add(d: DestinoViaje): void {
        this.destinos.push(d);
        // this.store.dispatch(new NuevoDestinoAction(d));
    }
    remove(d: DestinoViaje): void {
        const postion = this.destinos.indexOf(d);
        this.destinos.splice(postion, 1);
    }

    getAll(): DestinoViaje[] {
        return this.destinos;
    }

    getById(id: string): DestinoViaje {
        return this.destinos.filter(d => {
            return d.id.toString() === id;
        })[0];
    }

    elegir(e: DestinoViaje): any{
        this.destinos.forEach(x => x.setSelected(false));
        e.setSelected(true);
        this.current.next(e);
        // this.store.dispatch(new ElegidoFavoritoAction(e));
    }

    subscribeOnChange(fn): any{
        this.current.subscribe(fn);
    }

}
