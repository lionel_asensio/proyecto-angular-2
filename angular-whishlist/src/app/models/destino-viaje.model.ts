export class DestinoViaje {

  private selected: boolean;
  public servicios: string[];
  public id;

  constructor(public nombre: string, public imagenUrl: string,  public votes: number = 0) {
    this.servicios = ['pileta', 'desayuno'];
  }

  isSelected(): boolean{
    return this.selected;
  }

  setSelected(s: boolean): void{
    this.selected = s;
  }

  voteUp(): any {
    this.votes++;
  }

  voteDown(): any {
    this.votes--;
  }

}
