import { Component, Output, OnInit, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() itemAdded: EventEmitter<DestinoViaje>;

  fg: FormGroup;
  searchResults: string[];

  constructor(fb: FormBuilder) {
    this.itemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.required],
      url: ['']
    });

    this.fg.valueChanges.subscribe( (form: any) => {
      console.log('cambio en el form:', form);
    });

  }

  ngOnInit(): void {
    const elemNombre =  document.getElementById('nombre') as HTMLInputElement;
    fromEvent(elemNombre, 'input').pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => {
       return  text.length > 2;
      }),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(() => ajax('assets/datos.json'))
    ).subscribe(ajaxResponse => {
      this.searchResults = ajaxResponse.response;
    });
  }

  guardar(nombre: string, url: string): boolean{
    const d = new DestinoViaje(nombre, url);
    this.itemAdded.emit(d);
    return false;
  }

}

