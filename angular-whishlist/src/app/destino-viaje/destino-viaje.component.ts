import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { VoteUpAction, VoteDownAction } from '../models/destino-viajes-state';
import { AppState } from '../app.module';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
})
export class DestinoViajeComponent implements OnInit {

  @Input() destino: DestinoViaje;
  @Input() position: number;

  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>;
  @Output() removed: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter<DestinoViaje>();
    this.removed = new EventEmitter<DestinoViaje>();
  }

  ngOnInit(): void {}

  ir(): boolean{
    this.clicked.emit(this.destino);
    return false;
  }

  quitar(): boolean{
    this.removed.emit(this.destino);
    return false;
  }

  voteUp(): boolean {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown(): boolean {
    this.store.dispatch( new VoteDownAction(this.destino));
    return false;
  }


}
