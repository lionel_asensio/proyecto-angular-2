import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { DestinoApiClient } from '../models/destino-api-client';

import { Store } from '@ngrx/store';
import { AppState } from '../app.module';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
})
export class ListaDestinosComponent implements OnInit {

  @Output() itemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  // destinos: DestinoViaje[];

  constructor(public destinoApiClient: DestinoApiClient, private store: Store<AppState>) {
    // this.destinos = [];
    this.itemAdded = new  EventEmitter<DestinoViaje>();
    this.updates = [];

    this.destinoApiClient.subscribeOnChange((d: DestinoViaje) => {
      if (d != null){
        this.updates.push('Se ha elegido a ' + d.nombre.toString());
      }
    });

    // // Redux desactivado para poder seguir usando el output
    // this.store.select(state => state.destinos.favorito).subscribe(d => {
      // if (d != null) {
        // this.updates.push('Se ha elegido a ' + d.nombre);
      // }
    // });

  }

  ngOnInit(): void {}

  agregado(d: DestinoViaje): void {
    this.destinoApiClient.add(d);
    this.itemAdded.emit(d);

    // this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(e: DestinoViaje): void{
    // this.destinos.forEach((x) => {
      // x.setSelected(false);
    // });
    // this.destinoApiClient.getAll().forEach((x) => {
      // x.setSelected(false);
    // });
    // e.setSelected(true);

    this.destinoApiClient.elegir(e);

    //  this.store.dispatch(new ElegidoFavoritoAction(e));

  }

  remover(e: DestinoViaje): void{
    this.destinoApiClient.remove(e);
  }

}
